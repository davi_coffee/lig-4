let actualPlayer = 'player1';
let playerNext = ''

let playerBall = document.getElementById('playerBall');


function createColumns() {
  let tableDiv = document.getElementById('table');

  for (let i = 0; i < 7; i++) {
    let divColumn = document.createElement('div');
    divColumn.className = 'colunas';
    divColumn.addEventListener('click', mother);

    tableDiv.appendChild(divColumn);
  }
}
createColumns();

function mother(event) {
  let columnCurrent = event.currentTarget;

  if (columnCurrent.classList.contains('disabled')) {
    alertToClear();
    return;
  }

  if (columnCurrent.childElementCount < 6) {

    if (actualPlayer === 'player1') {
      playerNext = 'player2'

    } else {
      playerNext = 'player1'
    }

    let divDisc = document.createElement('div')
    divDisc.className = `cells ${actualPlayer}`
    columnCurrent.appendChild(divDisc)

    playerBall.classList.remove(actualPlayer)
    playerBall.classList.add(playerNext)

    if (checkWinner()) {
      roundWinnerModal(actualPlayer)
    } else if (checkDraw()) {
      roundDrawModal(actualPlayer)
    }

    actualPlayer = playerNext
  }
}
