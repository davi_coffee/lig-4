const cols = document.querySelectorAll('.colunas')

let scorePlayer1 = 0;
let scorePlayer2 = 0;

const addClassDisabled = () => {
    cols.forEach(v => v.classList.add('disabled'));
}

const removeClassDisabled = () => {
    cols.forEach(v => v.classList.remove('disabled'));
}

const closeModal = () => {
    document.querySelectorAll('.close').forEach(v => v.addEventListener('click', () => {
        document.querySelectorAll('.modal').forEach(v => v.classList.add('hide'))
    }))
}
closeModal();


function roundWinnerModal(player) {
    closeHide()
    document.getElementById('roundModalWinner').classList.remove('hide');

    let modalTitle = document.getElementById('modalTitle');
    modalTitle.textContent = `Vencedor - ${player.toUpperCase()}`;


    let ballModal = document.getElementById('ballModal')
    ballModal.className = `cells ${actualPlayer}`
    document.getElementById('roundModalWinner').appendChild(ballModal)

    if (player === 'player1') {
        scorePlayer1++;
        document.getElementById('scoreP1').textContent = scorePlayer1;
    } else {
        scorePlayer2++;
        document.getElementById('scoreP2').textContent = scorePlayer2;
    }
    addClassDisabled()
}

function roundDrawModal() {
    closeHide()
    document.getElementById('roundModalDraw').classList.remove('hide');

    addClassDisabled()
}

function alertToClear() {
    closeHide()
    document.getElementById('alertToClearModal').classList.remove('hide');

}


const btClearDesk = document.getElementById('clearDesk').addEventListener('click', function () {
    playerBall.className = `cells ${playerNext}`;

    document.querySelectorAll('.colunas > .cells').forEach(v => v.remove())
    // cols.forEach(v => v.addEventListener('click', mother))

    removeClassDisabled()

    closeHide()
});

function closeHide() {
    if (!document.getElementById('alertToClearModal').classList.contains('hide')) {
        document.getElementById('alertToClearModal').classList.add('hide')
    }
    if (!document.getElementById('roundModalDraw').classList.contains('hide')) {
        document.getElementById('roundModalDraw').classList.add('hide')
    }
    if (!document.getElementById('roundModalWinner').classList.contains('hide')) {
        document.getElementById('roundModalWinner').classList.add('hide')
    }
}

const btReset = document.getElementById('reset').addEventListener('click', function () {
    location.reload();
});

const iconInfo = document.querySelector('.far').addEventListener('click',
    function () {
        document.querySelector('.modalInfo').classList.remove('hide')
    })

const closeInfo = document.getElementById('closeInfo').addEventListener('click',
    function () {
        document.querySelector('.modalInfo').classList.add('hide')
    })